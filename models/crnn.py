import sys
import torch.nn as nn
from modules.feature_extraction import VGG_FeatureExtractor, RCNN_FeatureExtractor, ResNet_FeatureExtractor
# from classifier_crnn.modules.sequence_modeling import BidirectionalLSTM


class BidirectionalLSTM(nn.Module):
    def __init__(self, nIn, nHidden, nOut, dropout=0):
        super(BidirectionalLSTM, self).__init__()

        self.SequenceModeling = nn.LSTM(nIn, nHidden, bidirectional=True)
        self.embedding = nn.Linear(nHidden * 2, nOut)
        self.dropout = nn.Dropout(dropout)

    def forward(self, input):
        recurrent, _ = self.SequenceModeling(input)
        recurrent = self.dropout(recurrent)
        T, b, h = recurrent.size()
        t_rec = recurrent.view(T * b, h)

        output = self.embedding(t_rec)  # [T * b, nOut]
        output = output.view(T, b, -1)
        del recurrent
        del T
        del h
        del b
        del t_rec
        return output


class CRNN32(nn.Module):
    def __init__(self, imgH, nc, nclass, hidden_size, n_rnn=2, leakyRelu=False):
        super(CRNN32, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'

        ks = [3, 3, 3, 3, 3, 3, 2]
        ps = [1, 1, 1, 1, 1, 1, 0]
        ss = [1, 1, 1, 1, 1, 1, 1]
        nm = [64, 128, 256, 256, 512, 512, 512]
        cnn = nn.Sequential()

        def convRelu(i, batchNormalization=True):
            nIn = nc if i == 0 else nm[i - 1]
            nOut = nm[i]
            cnn.add_module('conv{0}'.format(i),
                           nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
            if batchNormalization:
                cnn.add_module('batchnorm{0}'.format(i), nn.BatchNorm2d(nOut))
            if leakyRelu:
                cnn.add_module('relu{0}'.format(i),
                               nn.LeakyReLU(0.2, inplace=True))
            else:
                cnn.add_module('relu{0}'.format(i), nn.ReLU(True))

        convRelu(0)
        cnn.add_module('pooling{0}'.format(
            0), nn.MaxPool2d(2, 2))  # 64, 16, 256
        convRelu(1)
        cnn.add_module('pooling{0}'.format(
            1), nn.MaxPool2d(2, 2))  # 128, 8, 128
        convRelu(2)
        convRelu(3)
        cnn.add_module('pooling{0}'.format(2), nn.MaxPool2d(
            (2, 2), (2, 2), (0, 0)))  # 256, 4, 64
        convRelu(4)
        convRelu(5)
        cnn.add_module('pooling{0}'.format(3),  nn.MaxPool2d(
            (2, 2), (2, 1), (0, 0)))  # 512, 2, 63
        convRelu(6)  # 512x1x16

        self.cnn = cnn
        self.rnn = nn.Sequential(
            BidirectionalLSTM(512, hidden_size, hidden_size, 0),
            BidirectionalLSTM(hidden_size, hidden_size, nclass, 0)
        )
        del ks
        del ps
        del ss
        del nm

    def forward(self, input):
        # conv features
        conv = self.cnn(input)

        b, c, h, w = conv.size()
        assert h == 1, "the height of conv must be 1"
        conv = conv.squeeze(2)
        conv = conv.permute(2, 0, 1)  # [w, b, c]

        # rnn features
        output = self.rnn(conv)

        return output


class CRNN64_VGG_like(nn.Module):
    def __init__(self, imgH, nc, nclass, hidden_size, n_rnn=2, backbone='VGG', leakyRelu=False):
        super(CRNN64_VGG_like, self).__init__()
        assert imgH % 16 == 0, 'imgH has to be a multiple of 16'

        def VGG_like():
            ks = [3, 3, 3, 3, 3, 3, 2, 2]
            ps = [1, 1, 1, 1, 1, 1, 1, 0]
            ss = [1, 1, 1, 1, 1, 1, 1, 1]
            nm = [64, 128, 256, 256, 512, 512, 512, 512]
            # print('Using VGG_like backbone')
            cnn = nn.Sequential()

            def convRelu(i, batchNormalization=False):
                nIn = nc if i == 0 else nm[i - 1]
                nOut = nm[i]
                cnn.add_module('dropout{0}'.format(i), nn.Dropout2d(p=0.2))
                cnn.add_module('conv{0}'.format(i),
                               nn.Conv2d(nIn, nOut, ks[i], ss[i], ps[i]))
                if batchNormalization:
                    cnn.add_module('batchnorm{0}'.format(
                        i), nn.BatchNorm2d(nOut))
                if leakyRelu:
                    cnn.add_module('relu{0}'.format(i),
                                   nn.LeakyReLU(0.2, inplace=True))
                else:
                    cnn.add_module('relu{0}'.format(i), nn.ReLU(True))
                del nIn
                del nOut

            convRelu(0, True)
            cnn.add_module('pooling{0}'.format(
                0), nn.MaxPool2d(2, 2))  # 64, 32, 256
            convRelu(1, True)
            cnn.add_module('pooling{0}'.format(
                1), nn.MaxPool2d(2, 2))  # 128, 16, 128
            convRelu(2, True)
            convRelu(3, True)
            cnn.add_module('pooling{0}'.format(2),
                           nn.MaxPool2d((2, 2), (2, 2), (0, 0)))  # 256, 8, 64
            convRelu(4, True)
            convRelu(5, True)
            cnn.add_module('pooling{0}'.format(3),
                           nn.MaxPool2d((2, 2), (2, 1), (0, 0)))  # 512, 4, 63
            convRelu(6, True)  # 512x1x16
            cnn.add_module('pooling{0}'.format(4),
                           nn.MaxPool2d((2, 2), (2, 1), (0, 0)))  # 512, 2, 63
            convRelu(7, True)
            return cnn

        if backbone == 'VGG':
            self.FeatureExtraction = VGG_like()
        # if backbone=='Resnet':
        #     self.cnn = Resnet()

        self.SequenceModeling = nn.Sequential(
            BidirectionalLSTM(512, hidden_size, hidden_size, 0.2),
            BidirectionalLSTM(hidden_size, hidden_size, nclass, 0.2))

    def forward(self, input):
        # conv features
        visual_feature = self.FeatureExtraction(input)  # 8,512,1,62
        b, c, h, w = visual_feature.size()
        assert h == 1, "the height of conv must be 1"
        visual_feature = visual_feature.squeeze(2)  # 8,512,62
        visual_feature = visual_feature.permute(2, 0, 1)  # 62,8,512

        # rnn features
        contextual_feature = self.SequenceModeling(visual_feature)  # 62,8,240
        del visual_feature
        del input
        del b, c, h, w
        return contextual_feature


class CRNN64_resnet(nn.Module):
    def __init__(self, input_channel, output_channel, hidden_size,
                 batch_max_length, num_class, featureExtraction, sequenceModeling):

        super(CRNN64_resnet, self).__init__()
        self.batch_max_length = batch_max_length
        self.stages = {'Feat': featureExtraction, 'Seq': sequenceModeling}

        """ FeatureExtraction """
        if featureExtraction == 'VGG':
            self.FeatureExtraction = VGG_FeatureExtractor(
                input_channel, output_channel)
        elif featureExtraction == 'RCNN':
            self.FeatureExtraction = RCNN_FeatureExtractor(
                input_channel, output_channel)
        elif featureExtraction == 'ResNet':
            print('Using resnet backbone')
            self.FeatureExtraction = ResNet_FeatureExtractor(
                input_channel, output_channel)
        else:
            raise Exception('No FeatureExtraction module specified')
        self.FeatureExtraction_output = output_channel  # int(imgH/16-1) * 512
        self.AdaptiveAvgPool = nn.AdaptiveAvgPool2d(
            (None, 1))  # Transform final (imgH/16-1) -> 1

        """ Sequence modeling"""
        if sequenceModeling == 'BiLSTM':
            self.SequenceModeling = nn.Sequential(
                BidirectionalLSTM(
                    output_channel, hidden_size, hidden_size, 0.2),
                BidirectionalLSTM(hidden_size, hidden_size, num_class, 0.2))
            self.SequenceModeling_output = num_class
        else:
            print('No SequenceModeling module specified')
            self.SequenceModeling_output = self.FeatureExtraction_output

    def forward(self, input, is_train=True):
        """ Feature extraction stage """
        visual_feature = self.FeatureExtraction(input)  # 8,512,3,129
        # visual_feature = self.AdaptiveAvgPool(visual_feature.permute(0, 3, 1, 2))  # 8,129,512,1
        # visual_feature = visual_feature.squeeze(3) #8,129,512

        b, c, h, w = visual_feature.size()
        assert h == 1, "the height of conv must be 1"
        visual_feature = visual_feature.squeeze(2)  # 8,512,62
        visual_feature = visual_feature.permute(2, 0, 1)  # 62,8,512

        """ Sequence modeling stage """
        if self.stages['Seq'] == 'BiLSTM':
            contextual_feature = self.SequenceModeling(
                visual_feature)  # 8,129,512
        else:
            # for convenience. this is NOT contextually modeled by BiLSTM
            contextual_feature = visual_feature

        return contextual_feature


if __name__ == '__main__':
    import torch
    model = CRNN64_resnet(3, 512, 256, 25, 37, 'ResNet', 'BiLSTM')
    model.eval()
    input = torch.randn(8, 3, 64, 100)
    output = model(input)
    print(output.size())
