import cv2
import torch
import gc
from datetime import datetime
import config_crnn as config_crnn
from data.char_list import alphabet
from models.utils import strLabelConverter
import models.utils as utils
import models.crnn as crnn
import numpy as np
from utils.loader import ImageFileLoader, NumpyListLoader
from utils.augment_functions2 import cnd_aug_resizePadding
from torchvision.transforms import ToTensor, Normalize
from torch.nn.functional import softmax
from torchvision import transforms
from torch.autograd import Variable
import matplotlib.patches as patches
from matplotlib import pyplot as plt
import os
import time

os.environ["LRU_CACHE_CAPACITY"] = "1"

# from aicr_utils.bbox_class import bbox

mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]
fill_color = (255, 255, 255)  # (209, 200, 193)

inv_normalize = transforms.Normalize(
    mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.225],
    std=[1 / 0.229, 1 / 0.224, 1 / 0.225])


class Classifier_CRNN:
    def __init__(self, ckpt_path='', gpu='1', batch_sz=16, workers=4, num_channel=3, imgW=1024, imgH=64, model_h=64,
                 char_list=alphabet, backbone='VGG_like', debug=False):
        self.imgW = imgW
        self.imgH = imgH
        self.batch_sz = batch_sz
        nclass = len(char_list) + 1
        self.image = torch.FloatTensor(batch_sz, num_channel, imgH, imgH)
        self.text = torch.IntTensor(batch_sz * 5)
        self.length = torch.IntTensor(batch_sz)
        self.backbone = backbone
        self.debug = debug
        if model_h == 32:
            self.model = crnn.CRNN32(imgH, num_channel, nclass, 256)
        else:
            if self.backbone == 'VGG_like':
                self.model = crnn.CRNN64_VGG_like(
                    imgH, num_channel, nclass, 256)

            if self.backbone == 'resnet':
                self.model = crnn.CRNN64_resnet(input_channel=num_channel, output_channel=512,
                                                hidden_size=256, num_class=nclass, batch_max_length=150,
                                                featureExtraction='ResNet', sequenceModeling='BiLSTM')
        if gpu != None and torch.cuda.is_available():
            print('Classifier_CRNN. Use GPU', gpu)
            os.environ['CUDA_VISIBLE_DEVICES'] = gpu
            self.model = self.model.cuda()
            self.image = self.image.cuda()
        else:
            kk = 1
            # print('Classifier_CRNN. Use CPU')

        # print('Classifier_CRNN. Load checkpoint %s' % ckpt_path)
        # state_dict=torch.load(ckpt_path, map_location='cpu')
        # for key in list(state_dict.keys()):
        #     if 'cnn' in key:
        #         state_dict[key.replace('cnn', 'FeatureExtraction')] = state_dict.pop(key)
        #     if 'rnn' in key:
        #         state_dict[key.replace('rnn', 'SequenceModeling')] = state_dict.pop(key)
        # torch.save(state_dict, ckpt_path)

        self.model.load_state_dict(torch.load(ckpt_path, map_location='cpu'))
        self.converter = strLabelConverter(char_list, ignore_case=False)
        self.image = Variable(self.image)
        self.text = Variable(self.text)
        self.length = Variable(self.length)
        self.workers = workers
        self.model.eval()

    def inference(self, *args):
        if len(args) == 1:
            numpy_img_list = args[0]
            max_wh_ratio = 2
            for img in numpy_img_list:
                # print(img.shape)
                wh_ratio = img.shape[1] / img.shape[0]
                if wh_ratio > max_wh_ratio:
                    max_wh_ratio = wh_ratio
            new_W = int(self.imgH * max_wh_ratio)
            # print('Classifier_CRNN. New W', new_W)
            transform_test = transforms.Compose([
                cnd_aug_resizePadding(
                    new_W, self.imgH, fill=fill_color, train=False),
                ToTensor(),
                Normalize(mean, std)
            ])

            val_dataset = NumpyListLoader(
                numpy_img_list, transform=transform_test)
        if len(args) == 3:
            dir, file_list, label = args[0], args[1], args[2]
            print('Classifier_CRNN. W', self.imgW)
            transform_test = transforms.Compose([
                cnd_aug_resizePadding(
                    self.imgW, self.imgH, fill=fill_color, train=False),
                ToTensor(),
                Normalize(mean, std)
            ])
            val_dataset = ImageFileLoader(
                dir, flist=file_list, label=label, transform=transform_test)
        return self.run_inference(val_dataset)

    def run_inference(self, val_dataset):
        num_files = len(val_dataset)
        if num_files == 0:
            return [], []
        # print('Classifier_CRNN. Begin classify', num_files, 'boxes')
        text_values = []
        prob_value = []
        val_loader = torch.utils.data.DataLoader(
            val_dataset,
            batch_size=self.batch_sz,
            num_workers=self.workers,
            shuffle=False
        )

        val_iter = iter(val_loader)
        max_iter = len(val_loader)
        begin = time.time()
        with torch.no_grad():
            for n in range(max_iter):
                data = val_iter.next()
                cpu_images, cpu_texts, img_paths = data
                batch_size = cpu_images.size(0)
                utils.loadData(self.image, cpu_images)
                preds = self.model(self.image)
                values, prob = softmax(preds, dim=-1).max(-1)
                preds_idx = (prob > 0).nonzero().squeeze(-1)
                psm = preds.softmax(2)
                preds_size = Variable(torch.IntTensor(
                    [preds.size(0)] * batch_size))
                _, preds = preds.max(2)
                new_psm, _ = psm.max(2)
                new_psm = new_psm.transpose(1, 0).contiguous().view(-1)
                preds = preds.transpose(1, 0).contiguous().view(-1)
                sim_pred, confidences = self.converter.decode_conf(preds.data, preds_size.data, new_psm, raw=False,
                                                                   rounding=4)

                if config_crnn.write_predict_file:
                    for idx, pred in enumerate(sim_pred):
                        final_pred = pred.lstrip().rstrip()
                        if config_crnn.include_conf:
                            conf = confidences[idx]
                            final_pred = final_pred + '\n' + str(conf)
                        pred_path = img_paths[idx]
                        for ext in ['jpg', 'JPG', 'png', 'PNG']:
                            pred_path = pred_path.replace('.' + ext, '.txt')
                        pred_time = datetime.today().strftime('%Y-%m-%d_%H-%M')
                        pred_path = pred_path.replace(
                            config_crnn.test_dir, config_crnn.test_dir + '_pred_'+pred_time)
                        dir = os.path.dirname(pred_path)
                        if not os.path.exists(dir):
                            os.makedirs(dir)
                        with open(pred_path, mode='w', encoding='utf-8') as f:
                            f.write(final_pred)

                text_values.extend(sim_pred)
                prob_value.extend(confidences)

                if self.debug:  # if debug then batch_size=1
                    raw_pred = self.converter.decode(
                        preds.data, preds_size.data, raw=True)
                    for i, text in enumerate(sim_pred):
                        try:
                            print('\n   ', raw_pred)
                            print(' =>', sim_pred[i], confidences)
                            # for idx, ch in enumerate(sim_pred[i]):
                            #     print(ch, confidences[idx])
                            # print('sent_prob', sent_prob)
                        except:
                            pass
                        inv_tensor = inv_normalize(cpu_images[i])
                        cv_img = inv_tensor.permute(1, 2, 0).numpy()
                        cv_img_convert = cv2.cvtColor(
                            cv_img, cv2.COLOR_BGR2RGB)
                        cv2.imshow('image data', cv_img_convert)
                        ch = cv2.waitKey(0)
                        if ch == 27:
                            break
        end = time.time()
        processing_time = end - begin
        # print('Classifier_CRNN. Processing time:', processing_time)
        # print('Classifier_CRNN. Speed:', num_files / processing_time, 'fps')
        del val_dataset
        del val_loader
        # del transform_test
        gc.collect()
        return text_values, prob_value

    def __call__(self, img_list):
        begin = time.time()
        rec_res = []
        values, probs = self.inference(img_list)
        for idx, val in enumerate(values):
            prob = sum(probs[idx])/len(probs[idx])
            rec_res.append([val, prob])
        end = time.time()
        elapse = end-begin
        return rec_res, elapse


root = os.path.dirname(__file__)
ckpt_path = os.path.join(root, 'weights/hw_2709.pth')
img_w = 512
img_h = 64
text_recognizer_vn = Classifier_CRNN(ckpt_path=ckpt_path,
                                     imgW=img_w,
                                     imgH=img_h,
                                     gpu=None)
# viz


def visualize_results(img_rgb, img_name, boxes_info, output_dir, text=True, inch=40):
    fig, ax = plt.subplots(1)
    fig.set_size_inches(inch, inch)
    img_gray = cv2.cvtColor(img_rgb, cv2.COLOR_RGB2GRAY)
    plt.imshow(img_gray, cmap='Greys_r')

    for box in boxes_info:
        if text:
            text_str = str(box.value)
            plt.text(box.xmin - 2, box.ymin - 4, text_str, fontsize=max(int(box.height / 3), 12),
                     fontdict={"color": 'r'})

        ax.add_patch(patches.Rectangle((box.xmin, box.ymin), box.width, box.height,
                                       linewidth=2, edgecolor='green', facecolor='none'))
    save_img_path = os.path.join(output_dir, img_name.split('.')[
                                 0] + '_visualized.jpg')
    print('Save visualized result to', save_img_path)
    fig.savefig(save_img_path, bbox_inches='tight')


# def sample_codes():
#     import cv2
#     from classifier_crnn.crnn_class import Classifier_CRNN
#     img_data = cv2.imread('data/sample.jpg')
#     classifier = Classifier_CRNN(ckpt_path=config_crnn.pretrained_test, imgW=512, imgH=64, gpu='0')
#     values, probs = classifier.inference([img_data])
#     print(values, probs)

def test_inference():
    engine = Classifier_CRNN(ckpt_path=config_crnn.pretrained_test, batch_sz=config_crnn.batch_size_test,
                             imgW=config_crnn.imgW, imgH=config_crnn.imgH, gpu=config_crnn.gpu_test,
                             char_list=alphabet, backbone=config_crnn.backbone, debug=config_crnn.debug)

    begin = time.time()
    a, b = engine.inference(config_crnn.test_dir,
                            config_crnn.test_list, config_crnn.label)
    end = time.time()
    print('Inference time:', end - begin, 'seconds')


if __name__ == "__main__":
    # ample_codes()
    test_inference()
