from prepare_crnn_data import get_list_file_in_dir_and_subdirs
from models.utils import cer_loss_one_image
import os
import ast
import cv2


def eval_cer_loss(pred_dir, gt_dir):
    gt_files = get_list_file_in_dir_and_subdirs(gt_dir, ext=['txt'])
    num_files = len(gt_files)
    total_loss = 0
    for file in gt_files:
        with open(os.path.join(gt_dir, file), 'r', encoding='utf-8') as f:
            gt = f.readlines()[0].replace('\n', '')
        with open(os.path.join(pred_dir, file), 'r', encoding='utf-8') as f:
            pred = f.readlines()[0].replace('\n', '')
        loss = cer_loss_one_image(pred, gt)
        total_loss += loss
    print('CER:', total_loss / num_files)


def eval_accuracy(pred_dir, gt_dir, img_dir=None, conf_thres=0.9999):
    print('eval_accuracy. conf_thres', conf_thres)
    gt_files = get_list_file_in_dir_and_subdirs(gt_dir, ext=['txt'])
    num_files = len(gt_files)
    num_high_conf = 0
    num_high_conf_correct = 0
    num_low_conf = 0
    num_low_conf_correct = 0
    for file in gt_files:
        print(file)
        with open(os.path.join(gt_dir, file), 'r', encoding='utf-8') as f:
            lines = f.readlines()
            gt = ''
            if len(lines) > 0:
                gt = lines[0].replace('\n', '')

        with open(os.path.join(pred_dir, file), 'r', encoding='utf-8') as f:
            lines = f.readlines()
            pred = lines[0].replace('\n', '')
            confidences = ast.literal_eval(lines[1].replace('\n', ''))
        line_conf = 1.0
        if len(confidences) > 0:
            line_conf = sum(confidences) / len(confidences)
        if img_dir is not None:
            img = cv2.imread(os.path.join(
                gt_dir, file.replace('.txt', '.jpg')))
            if pred == gt:
                print('pred', pred)
                print('gt', gt)
                cv2.imshow('result', img)
                ch = cv2.waitKey(0)
                if ch == 27:
                    break
        if line_conf >= conf_thres:
            num_high_conf += 1
            if pred == gt:
                num_high_conf_correct += 1
        else:
            num_low_conf += 1
            if pred == gt:
                num_low_conf_correct += 1

    num_correct_total = num_high_conf_correct + num_low_conf_correct
    print('num_high_conf_correct', num_high_conf_correct, 'num_high_conf', num_high_conf, 'acc',
          num_high_conf_correct / num_high_conf)
    print('num_low_conf_correct', num_low_conf_correct, 'num_low_conf', num_low_conf, 'acc',
          num_low_conf_correct / num_low_conf)
    print('num_correct_total', num_correct_total, 'num_files',
          num_files, 'acc', num_correct_total / num_files)


if __name__ == "__main__":
    pred_dir = '/usb/sdb/data/aicr/train_data_29Feb_update_30Mar_13May_refined_03Sep/sevt_form1/real/test_pred_2020-09-24_17-12'
    gt_dir = '/usb/sdb/data/aicr/train_data_29Feb_update_30Mar_13May_refined_03Sep/sevt_form1/real/test'
    eval_accuracy(pred_dir, gt_dir)
    # eval_cer_loss(pred_dir, gt_dir)
