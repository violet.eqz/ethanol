import math, cv2
import numpy as np


def distance_from_pts_to_line(x0, y0, a, b, c):
    return abs(a * x0 + b * y0 + c) / math.sqrt(a * a + b * b)

def euclidean_distance(pt1, pt2):
    return math.sqrt((pt1[0] - pt2[0]) * (pt1[0] - pt2[0]) + (pt1[1] - pt2[1]) * (pt1[1] - pt2[1]))


def order_points(pts):
    """
    reference from: https://github.com/jrosebr1/imutils/blob/master/imutils/perspective.py
    # sort the points based on their x-coordinates
    """
    xSorted = pts[np.argsort(pts[:, 0]), :]

    # grab the left-most and right-most points from the sorted
    # x-roodinate points
    leftMost = xSorted[:2, :]
    rightMost = xSorted[2:, :]

    # now, sort the left-most coordinates according to their
    # y-coordinates so we can grab the top-left and bottom-left
    # points, respectively
    leftMost = leftMost[np.argsort(leftMost[:, 1]), :]
    (tl, bl) = leftMost

    rightMost = rightMost[np.argsort(rightMost[:, 1]), :]
    (tr, br) = rightMost

    rect = np.array([tl, tr, br, bl], dtype="float32")
    return rect


def resize_normalize(img, normalize_width=1000):
    w = img.shape[1]
    h = img.shape[0]
    resize_ratio = normalize_width / w
    normalize_height = round(h * resize_ratio)
    resize_img = cv2.resize(img, (normalize_width, normalize_height), interpolation=cv2.INTER_CUBIC)
    # cv2.imshow('resize img', resize_img)
    # cv2.waitKey(0)
    return resize_ratio, resize_img

class text_box():
    def __init__(self, segment_pts, type=1, value=''):
        if isinstance(segment_pts, str):
            segment_pts = [int(f) for f in segment_pts.split(',')]
        elif isinstance(segment_pts, list):
            segment_pts = [round(f) for f in segment_pts]
        self.type = type
        self.value = value
        num_pts = int(len(segment_pts) / 2)
        if num_pts != 4:
            print('text_box is not qualitareal!')
        first_pts = [segment_pts[0], segment_pts[1]]
        self.list_pts = [first_pts]
        for i in range(1, num_pts):
            self.list_pts.append([segment_pts[2 * i], segment_pts[2 * i + 1]])

        # init confident for detector and recognizer
        self.conf_det = -1
        self.conf_rec = -1
        self.center = []
        self.params = []  # a,b in equation y = ax +b

        # Thêm trường cho kie
        self.kie_res = None
        self.conf_kie = -1

    def check_max_wh_ratio(self):
        '''
        find longer edge + shorter edge and calculate long edge
        :return:
        '''
        max_ratio = 0
        assert len(self.list_pts) == 4
        first_edge = euclidean_distance(self.list_pts[0], self.list_pts[1])
        second_edge = euclidean_distance(self.list_pts[1], self.list_pts[2])
        if first_edge / second_edge > 1:
            long_edge = (self.list_pts[0][0] - self.list_pts[1][0], self.list_pts[0][1] - self.list_pts[1][1])
            short_edge = (self.list_pts[1][0] - self.list_pts[2][0], self.list_pts[1][1] - self.list_pts[2][1])
        else:
            long_edge = (self.list_pts[1][0] - self.list_pts[2][0], self.list_pts[1][1] - self.list_pts[2][1])
            short_edge = (self.list_pts[0][0] - self.list_pts[1][0], self.list_pts[0][1] - self.list_pts[1][1])
        max_ratio = max(first_edge / second_edge, second_edge / first_edge)
        return max_ratio, long_edge, short_edge

    def calculate_width_height_center(self):
        assert len(self.list_pts) == 4
        first_edge = euclidean_distance(self.list_pts[0], self.list_pts[1])
        second_edge = euclidean_distance(self.list_pts[1], self.list_pts[2])
        # assume that width always bigger than height
        self.width, self.height = max(first_edge, second_edge), min(first_edge, second_edge)
        self.center = [int((self.list_pts[0][0] + self.list_pts[1][0] + self.list_pts[2][0] + self.list_pts[3][0]) / 4),
                       int((self.list_pts[0][1] + self.list_pts[1][1] + self.list_pts[2][1] + self.list_pts[3][1]) / 4)]

    def is_horizontal_box(self):
        '''
        check if a box is horizontal box or not
        :return:
        '''
        angle_with_horizontal_line = self.get_horizontal_angle()
        if math.fabs(angle_with_horizontal_line) > 45 and math.fabs(angle_with_horizontal_line) < 135:
            return False
        else:
            return True

    def get_horizontal_angle(self):
        assert len(self.list_pts) == 4
        max_ratio, long_edge, short_edge = self.check_max_wh_ratio()
        if long_edge[0] == 0:
            if long_edge[1] < 0:
                angle_with_horizontal_line = -90
            else:
                angle_with_horizontal_line = 90
        else:
            angle_with_horizontal_line = math.atan2(long_edge[1], long_edge[0]) * 57.296
        return angle_with_horizontal_line

    def get_params_of_text_box(self):
        '''
        # get a,b in equation y = ax + b. this straight line go throuh centor of box

        :return:
        '''
        if self.center == []:
            self.calculate_width_height_center()

        if self.list_pts[0][0] != self.list_pts[1][0]:
            a = (self.list_pts[0][1] - self.list_pts[1][1]) / (self.list_pts[0][0] - self.list_pts[1][0])
            b = self.list_pts[0][1] - a * self.list_pts[0][0]
            self.params = [a, b]

    def to_icdar_line(self, value=False, type=False):
        line_str = ''
        if len(self.list_pts) == 4:
            for pts in self.list_pts:
                line_str += '{},{},'.format(pts[0], pts[1])
            line_str = line_str.rstrip(',')
            if value:
                line_str += ',' + self.value
            if type:
                line_str += ',' + str(self.type)

        else:
            print('to_icdar_line. text_box is not qualitareal')
        return line_str

    def add_offset(self, offset_x=0, offset_y=0):
        '''
        offset toa do
        :param offset_x:
        :param offset_y:
        :return:
        '''
        res_pts = []
        for pt in self.list_pts:
            res_pts.append([pt[0] + offset_x, pt[1] + offset_y])
        self.list_pts = res_pts


def rotate_and_crop(img, points, debug=False, extend=True,
                    extend_x_ratio=1, extend_y_ratio=0.01,
                    min_extend_y=1, min_extend_x=2, wh_thres = 15):
    '''

    :param img:
    :param points: list [4,2]
    :param debug:
    :param extend:
    :param extend_x_ratio:
    :param extend_y_ratio:
    :param min_extend_y:
    :param min_extend_x:
    :return:
    '''

    default_val = {'min_extend_x':2,
                   'extend_x_ratio':0.1,
                   'min_extend_y':2,
                   'extend_y_ratio':0.1}

    ### Warning: points must be sorted clockwise
    points = order_points(np.asarray(points))

    w = int(euclidean_distance(points[0], points[1]))
    h = int(euclidean_distance(points[1], points[2]))

    if w/h > wh_thres:
        min_extend_x = default_val['min_extend_x']
        extend_x_ratio = default_val['extend_x_ratio']
        min_extend_y = default_val['min_extend_y']
        extend_y_ratio = default_val['extend_y_ratio']

    # get width and height of the detected rectangle

    if extend:
        ex = min_extend_x if (extend_x_ratio * h) < min_extend_x else (extend_x_ratio * h)
        ey = min_extend_y if (extend_y_ratio * h) < min_extend_y else (extend_y_ratio * h)
        ex = int(round(ex))
        ey = int(round(ey))
    else:
        ex, ey = 0, 0
    src_pts = points.astype("float32")
    dst_pts = np.array([
        [ex, ey],
        [w - 1 + ex, ey],
        [w - 1 + ex, h - 1 + ey],
        [ex, h - 1 + ey]
    ], dtype="float32")

    M = cv2.getPerspectiveTransform(src_pts, dst_pts)

    warped = cv2.warpPerspective(img, M, (w + 2 * ex, h + 2 * ey))
    # if warped.mean() < 145:
    #     warped = 255 - warped

    if debug:
        print('wh_ratio, ex, ey', round(w/h,2), ex, ey)
        cv2.imshow('rotate and extend', warped)
        cv2.waitKey(0)
    return warped


def get_list_imgs_from_text_boxes(img, list_text_boxes, extend_box, extend_x_ratio, extend_y_ratio, min_extend_x, min_extend_y):
    '''
    lấy ra list ảnh từ thông tin text box
    :param input:  'line' or 'text_box'
    :param extend_box:
    :param extend_x_ratio:
    :param extend_y_ratio:
    :param min_extend_x:
    :param min_extend_y:
    :return:
    '''
    list_imgs = []
    for box in list_text_boxes:
        box.img = rotate_and_crop(img, box.list_pts, debug=False,
                                  extend=extend_box,
                                  extend_x_ratio=extend_x_ratio, extend_y_ratio=extend_y_ratio,
                                  min_extend_y=min_extend_y, min_extend_x=min_extend_x)
        # cv2.imshow('img', box.img)
        # cv2.waitKey(0)
        list_imgs.append(box.img)
    return list_imgs