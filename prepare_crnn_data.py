import os
import cv2
import random
import numpy as np
import shutil
from utils.unicode_utils import compound_unicode
from data.char_list import alphabet
from common import resize_normalize

icdar_dir = '/home/aicr/cuongnd/aicr.core/data_generator/outputs/corpus_1000_2020-05-22_10-37/images'
output_dir = '/home/aicr/cuongnd/aicr.core/data_generator/outputs/corpus_1000_2020-05-22_10-37/ocr_invoices'


class bbox:
    def __init__(self, label, x1, x2, y1, y2):
        self.x1 = x1
        self.x2 = x2
        self.y1 = y1
        self.y2 = y2
        self.label = label


def get_list_file_in_folder(dir, ext=['jpg', 'png', 'JPG', 'PNG']):
    included_extensions = ext
    file_names = [fn for fn in os.listdir(dir)
                  if any(fn.endswith(ext) for ext in included_extensions)]
    return file_names


def get_list_dir_in_folder(dir):
    sub_dir = [o for o in os.listdir(
        dir) if os.path.isdir(os.path.join(dir, o))]
    return sub_dir


def get_list_file_in_dir_and_subdirs(folder, ext=['jpg', 'png', 'JPG', 'PNG']):
    file_names = []
    for path, subdirs, files in os.walk(folder):
        for name in files:
            extension = os.path.splitext(name)[1].replace('.', '')
            if extension in ext:
                file_names.append(os.path.join(
                    path, name).replace(folder, '')[1:])
                # print(os.path.join(path, name).replace(folder,'')[1:])
    return file_names


def get_list_dir_and_subdirs_in_folder(folder):
    list_dir = [x[0].replace(folder, '').lstrip('/') for x in os.walk(folder)]
    return list_dir


def fix_unicode(txt_file):
    print('fix unicode', txt_file)

    with open(txt_file, 'r', encoding='utf-8') as f:
        txt = f.readlines()
    final_str = ''
    for anno in txt:
        is_write = True
        for c in anno:
            if c not in alphabet and c != '\n':
                print(c)
                is_write = False
                continue
        if is_write:
            final_str += anno
    if len(txt) > 0:
        new_text = compound_unicode(final_str)
        with open(txt_file, 'w', encoding='utf-8') as f:
            f.write(new_text)


def prepare_train_test(root_dir, extensions=['jpg', 'png', 'JPG', 'PNG']):
    train_list = []
    test_list = []
    print('root dir:', root_dir)

    list_dir = get_list_dir_and_subdirs_in_folder(root_dir)
    from data.char_list import alphabet
    max_ratio = 1
    for dir in list_dir:
        if dir.endswith('train') or dir.endswith('test'):
            list_files = get_list_file_in_dir_and_subdirs(
                os.path.join(root_dir, dir))
            print('Dir ', dir, 'has total', len(list_files), 'files')
            for idx, file in enumerate(list_files):
                # print(file)
                # fix unicode
                txt_file = os.path.join(root_dir, dir, file)
                try:
                    img = cv2.imread(txt_file)
                    ratio = img.shape[1]/img.shape[0]
                    if ratio > max_ratio:
                        max_ratio = ratio
                except:
                    print('Something wrong', txt_file)
                for ext in extensions:
                    txt_file = txt_file.replace('.' + ext, '.txt')
                use_to_train_test = True
                if os.path.exists(txt_file):
                    with open(txt_file, 'r', encoding='utf8', errors='ignore') as f:
                        txt = f.readlines()
                    if len(txt) > 0:
                        new_text = compound_unicode(txt[0])
                        for ch in txt[0]:
                            if ch not in alphabet:
                                print(ch, file)
                                use_to_train_test = False
                                # os.remove(os.path.join(root_dir,dir, file))
                                break
                        with open(txt_file, 'w', encoding='utf-8') as f:
                            f.write(new_text)
                else:
                    print('label not exist!', txt_file)
                if use_to_train_test:
                    if dir.endswith('train'):
                        train_list.append(os.path.join(dir, file))
                    if dir.endswith('test'):
                        test_list.append(os.path.join(dir, file))
                else:
                    print('ignore!')
    print('Max ratio', max_ratio)
    # random.shuffle(train_list)
    # random.shuffle(test_list)
    print('\ntrain files:', len(train_list))
    print('\ntest files:', len(test_list))

    print('Write train test file')
    train_txt = ''
    for train_file in train_list:
        train_txt += train_file + '\n'
    with open(os.path.join(root_dir, 'train.txt'), 'w', encoding='utf-8') as f:
        f.write(train_txt)

    test_txt = ''
    for test_file in test_list:
        test_txt += test_file + '\n'
    with open(os.path.join(root_dir, 'val.txt'), 'w', encoding='utf-8') as f:
        f.write(test_txt)

    print('Done')


def separate_train_test_from_dir(data_dir, percentage=1.0, train_ratio=0.9):
    if os.path.exists(os.path.join(data_dir, 'train')) or os.path.exists(os.path.join(data_dir, 'test')):
        print('Already separated!')
        return
    list_dir = get_list_dir_and_subdirs_in_folder(data_dir)
    for dir in list_dir:
        print('\nSeparate train test in', os.path.join(data_dir, dir))
        train_dir = os.path.join('train', dir)
        test_dir = os.path.join('test', dir)
        if not os.path.exists(os.path.join(data_dir, train_dir)):
            os.makedirs(os.path.join(data_dir, train_dir))
        if not os.path.exists(os.path.join(data_dir, test_dir)):
            os.makedirs(os.path.join(data_dir, test_dir))
        list_files = get_list_file_in_folder(os.path.join(data_dir, dir))
        random.shuffle(list_files)

        num_files_to_use = int(percentage * len(list_files))
        num_files_to_train = int(train_ratio * num_files_to_use)
        num_files_to_test = num_files_to_use - num_files_to_train
        print('train files:', num_files_to_train)
        print('test files:', num_files_to_test)
        print('start separating...')

        train_list = list_files[0:num_files_to_train]
        test_list = list_files[num_files_to_train:num_files_to_use]
        for idx, train_file in enumerate(train_list):
            src_file = os.path.join(os.path.join(data_dir, dir, train_file))
            dst_file = os.path.join(os.path.join(
                data_dir, train_dir, train_file))
            print(idx, os.path.join(os.path.join(dir, train_file)),
                  ' --> ', os.path.join(train_dir, train_file))
            shutil.move(src_file, dst_file)
            if os.path.exists(os.path.splitext(src_file)[0] + '.txt'):
                shutil.move(os.path.splitext(src_file)[
                            0] + '.txt', os.path.splitext(dst_file)[0] + '.txt')

        for idx, test_file in enumerate(test_list):
            src_file = os.path.join(os.path.join(data_dir, dir, test_file))
            dst_file = os.path.join(os.path.join(
                data_dir, test_dir, test_file))
            print(idx, os.path.join(os.path.join(dir, test_file)),
                  ' --> ', os.path.join(test_dir, test_file))
            shutil.move(src_file, dst_file)
            if os.path.exists(os.path.splitext(src_file)[0] + '.txt'):
                shutil.move(os.path.splitext(src_file)[
                            0] + '.txt', os.path.splitext(dst_file)[0] + '.txt')

    # delete old dir
    for dir in list_dir:
        if dir != '':
            try:
                shutil.rmtree(os.path.join(data_dir, dir))
            except FileNotFoundError:
                pass


def crop_from_img_rectangle(img, left, top, right, bottom, extend=True, extend_y_ratio=0.05, min_extend_y=4):
    extend_y = 0
    if extend:
        extend_y = max(int(extend_y_ratio*(bottom-top)), min_extend_y)
    # print('extend y:',extend_y)
    extend_x = 0
    top = max(0, top - extend_y)
    bottom = min(img.shape[0], bottom + extend_y)
    left = max(0, left - extend_x)
    right = min(img.shape[1], right + extend_x)
    if left >= right or top >= bottom or left < 0 or top < 0 or left >= img.shape[1] or right >= img.shape[1]:
        return True, None, -1, -1, -1, -1
    return False, img[top:bottom, left:right], left, top, right, bottom


# icdar_format: True if icdar or False if yolo
def prepare_train_crnn_from_icdar(data_dir, output_dir='', same_folder=True, icdar_format=True):
    if output_dir == '':
        output_dir = data_dir
    try:
        os.makedirs(os.path.join(output_dir, 'images'))
        os.makedirs(os.path.join(output_dir, 'annos'))
    except:
        pass

    list_files = get_list_file_in_folder(data_dir)
    train_txt = ''
    test_txt = ''
    max_wh = 0
    count = 0
    for idx, file in enumerate(list_files):
        print(idx, file)
        base_name = file.replace('.png', '').replace('.jpg', '')
        img_path = os.path.join(data_dir, file)
        img = cv2.imread(img_path)
        anno_path = img_path.replace('.png', '.txt').replace('.jpg', '.txt')
        with open(anno_path, 'r', encoding='utf-8') as f:
            anno_list = f.readlines()
        anno_list = [x.strip() for x in anno_list]
        for index, anno in enumerate(anno_list):
            if icdar_format:
                pts = anno.split(',')
                left = int(pts[0])
                top = int(pts[1])
                right = int(pts[2])
                bottom = int(pts[5])
            else:
                pts = anno.split(' ')
                left = int(pts[1])
                top = int(pts[2])
                right = int(pts[1]) + int(pts[3])
                bottom = int(pts[2]) + int(pts[4])
            loc = -1
            for i in range(0, 8):
                loc = anno.find(',', loc + 1)
            val = anno[loc + 1:]

            if val.isdigit():
                continue

            # remove many space
            for i in range(10):
                val = val.replace('  ', ' ')

            NG, crop, new_left, new_top, new_right, new_bottom = crop_from_img_rectangle(
                img, left, top, right, bottom, extend=True)
            if crop is None or val == '':
                continue
            if ((crop.shape[1] / crop.shape[0]) > max_wh):
                max_wh = crop.shape[1] / crop.shape[0]

            base_crop_name = base_name + '/' + str(index)
            if not os.path.exists(os.path.join(output_dir, 'images', base_name)):
                os.makedirs(os.path.join(output_dir, 'images', base_name))
            if (random.randint(0, 4) == 0):
                test_txt += 'images/' + base_crop_name + '.jpg\n'
            else:
                train_txt += 'images/' + base_crop_name + '.jpg\n'
            crop_img_path = os.path.join(
                output_dir, 'images', base_crop_name + '.jpg')
            crop_anno_path = os.path.join(
                output_dir, 'annos', base_crop_name + '.txt')
            cv2.imwrite(crop_img_path, crop)
            if same_folder:
                crop_anno_path = crop_img_path.replace('.jpg', '.txt')
            # with open(crop_anno_path, 'w', encoding='utf-8') as f:
            #     f.write(val)
            count += 1

    with open(os.path.join(output_dir, 'train.txt'), 'w', encoding='utf-8') as f:
        f.write(train_txt)
    with open(os.path.join(output_dir, 'val.txt'), 'w', encoding='utf-8') as f:
        f.write(test_txt)
    print('max width height ratio in dataset', max_wh)
    print('Total word:', count)


def prepare_txt_file(data_dir):
    list_files = get_list_file_in_folder(data_dir)
    save_txt = ''
    for file in list_files:
        save_txt += os.path.join(data_dir, file) + '\n'
        with open(os.path.join(data_dir, file.replace('.jpg', '.txt').replace('.png', '.txt')), 'w',
                  encoding='utf-8') as f:
            f.write('abc')
    with open(os.path.join(data_dir + '/..', 'test'), 'w', encoding='utf-8') as f:
        f.write(save_txt)


def convert_json_to_multiple_gt(dir, json_name='labels.json'):
    import json
    with open(os.path.join(dir, json_name)) as json_file:
        data = json.load(json_file)
        for key, value in data.items():
            gt_name = key.replace('.jpg', '.txt').replace('.png', '.txt')
            with open(os.path.join(dir, gt_name), 'w', encoding='utf-8') as f:
                f.write(value)


def crop_collected_data(dir, file_list=['2', '8', '14', '20'], debug=False):
    list_files = get_list_file_in_folder(dir)
    for file in list_files:
        if file.replace('.jpg', '') in file_list:
            file_path = os.path.join(dir, file)
            print(file_path)
            ori_img = cv2.imread(file_path)
            crop_img = ori_img[0:82, 0:ori_img.shape[1]]
            if debug:
                cv2.imshow('result', crop_img)
                cv2.waitKey(0)
            cv2.imwrite(file_path, crop_img)


def crop_collected_data2(dir, debug=False):
    list_files = get_list_file_in_folder(dir)
    count1 = 0
    count2 = 0
    for idx, file in enumerate(list_files):
        if (idx > 16):
            continue
        file_path = os.path.join(dir, file)
        print(file_path)
        ori_img = cv2.imread(file_path)
        if ori_img.shape[1] / ori_img.shape[0] > 9:
            count1 += 1
            extend_val = int(ori_img.shape[0] / 9) + 3
            print('count1', count1, 'extend val',
                  extend_val, 'height', ori_img.shape[0])
            crop_img = ori_img[extend_val:ori_img.shape[0] -
                               extend_val - 1, 0:ori_img.shape[1]]
            if debug:
                # cv2.imshow('ori', ori_img)
                cv2.imshow('result', crop_img)
                cv2.waitKey(0)
            cv2.imwrite(file_path, crop_img)
        elif ori_img.shape[1] / ori_img.shape[0] > 7:
            count2 += 1
            extend_val = int(ori_img.shape[0] / 9) + 1
            print('count2', count2, 'extend val',
                  extend_val, 'height', ori_img.shape[0])
            crop_img = ori_img[extend_val:ori_img.shape[0] -
                               extend_val - 1, 0:ori_img.shape[1]]
            if debug:
                # cv2.imshow('ori', ori_img)
                cv2.imshow('result', crop_img)
                cv2.waitKey(0)
            cv2.imwrite(file_path, crop_img)


def gen_blank_image(target_dir, num=150):
    for i in range(num):
        h = random.randint(32, 150)
        w = random.randint(int(h / 2), int(10 * h))
        print(i, w, h)

        blank_img = np.zeros([h, w, 3], dtype=np.uint8)
        blank_img.fill(255)
        cv2.imwrite(os.path.join(target_dir, str(i) + '.jpg'), blank_img)
        with open(os.path.join(target_dir, str(i) + '.txt'), 'w') as f:
            f.write('')


def is_overlapped(box_a, box_b, iou_thres=0.9):
    intersecting_area = max(0, min(box_a.x2, box_b.x2) - max(box_a.x1, box_b.x1)) * \
        max(0, min(box_a.y2, box_b.y2) - max(box_a.y1, box_b.y1))
    box_a_area = (box_a.x2 - box_a.x1) * (box_a.y2 - box_a.y1)
    box_b_area = (box_b.x2 - box_b.x1) * (box_b.y2 - box_b.y1)
    percent_coverage = intersecting_area / \
        (box_a_area + box_b_area - intersecting_area)
    if percent_coverage > iou_thres:
        print('overlap')
        return True
    else:
        return False


def refine_anno(DB_anno_dir, keyDet_anno_dir, refine_anno_dir, filename):
    DB_anno_txt = open(os.path.join(DB_anno_dir, filename),
                       encoding='utf8').read().rstrip()
    keyDet_anno_txt = open(os.path.join(
        keyDet_anno_dir, filename), encoding='utf8').read().rstrip()
    DB_anno_list = DB_anno_txt.split('\n')
    keyDet_anno_list = keyDet_anno_txt.split('\n')

    keyDet_bbox_list = []
    refine_bbox_list = []
    for line in keyDet_anno_list:
        label, x, y, w, h = line.split(' ')
        new_bbox = bbox(label, int(x), int(
            x) + int(w), int(y), int(y) + int(h))
        keyDet_bbox_list.append(new_bbox)
        refine_bbox_list.append(new_bbox)

    for line in DB_anno_list:
        label, x, y, w, h = line.split(' ')
        db_bbox = bbox('text', int(x), int(
            x) + int(w), int(y), int(y) + int(h))
        is_overlap = False
        for box in keyDet_bbox_list:
            if is_overlapped(db_bbox, box):
                is_overlap = True
        if not is_overlap:
            refine_bbox_list.append(db_bbox)

    refine_anno_txt = ''
    for box in refine_bbox_list:
        line = ' '.join([box.label, str(box.x1), str(box.y1),
                        str(box.x2 - box.x1), str(box.y2 - box.y1)])
        refine_anno_txt += line + '\n'

    refine_anno_txt = refine_anno_txt.rstrip('\n')
    with open(os.path.join(refine_anno_dir, filename), 'w', encoding='utf-8') as f:
        f.write(refine_anno_txt)


def prepare_anno_field_extractor(txt_file):
    with open(txt_file, 'r', encoding='utf-8') as f:
        txt = f.readlines()
    final_str = ''
    old_str = []
    for anno in txt:
        if anno not in old_str:
            old_str.append(anno)
            final_str += '__label__  ' + anno
    if len(txt) > 0:
        new_text = compound_unicode(final_str)
        with open(txt_file, 'w', encoding='utf-8') as f:
            f.write(new_text)


def refine_ede_txt(file_path, dir=''):
    from data.char_list import alphabet_ede
    anno_txt = (open(os.path.join(dir, file_path),
                encoding='utf8').read().rstrip()).split('\n')
    final_txt = []
    for txt in anno_txt:
        new_txt = txt.strip().strip('.').strip(',')
        new_text = compound_unicode(new_txt)
        print(new_text)
        append = True
        for ch in new_text:
            if ch not in alphabet_ede:
                print(ch)
                append = False
                break
        if append:
            final_txt.append(new_text)
    final_text = ''
    for txt in final_txt:
        final_text += txt+'\n'

    with open('/home/cuongnd/PycharmProjects/aicr/aicr.core2/data_processing/TextRecognitionDataGenerator_ede/trdg/dicts/ede_fixed.txt', 'w', encoding='utf-8') as f:
        f.write(final_text)


def refine_testcase(dir, dst_dir):
    list_files = get_list_file_in_dir_and_subdirs(dir)
    for img in list_files:
        print(img)
        src_img = cv2.imread(os.path.join(dir, img))
        if src_img.shape[1] > 1200:
            _, src_img = resize_normalize(src_img, normalize_width=1200)
        dir_file = os.path.dirname(os.path.join(dst_dir, img))
        if not os.path.exists(dir_file):
            os.makedirs(dir_file)
        cv2.imwrite(os.path.join(dst_dir, img).replace(
            '.PNG', '.jpg').replace('.png', '.jpg'), src_img)


if __name__ == "__main__":
    # refine_testcase('/data/data_MireaAsset/failed_OoS_testcases', '/data/data_MireaAsset/failed_resize')
    # separate_train_test_from_dir('/data20.04/data/aicr/ocr_dataset', percentage=1.0)
    prepare_train_test('/data20.04/data/aicr/ocr_dataset')
    src_dir = '/data/English_test/result_Korea_30July/output_textline_classifier'
    dst_dir = '/data/English_test/result_Korea_30July/output_res'
    # prepare_train_crnn_from_icdar('/data20.04/data/data_invoice/text_yolo_refine_13Aug_new/kk', output_dir='/data20.04/data/data_invoice/text_yolo_refine_13Aug_new/94_crnn', icdar_format=False)
