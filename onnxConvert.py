import torch.onnx
import onnx
import torchvision

# import aicr_configs as global_config
import aicr_configs.sevt.config as sevtconfig
from data.char_list import alphabet
import models.crnn as crnn

imgH = 64
num_channel = 3
batchSize = 64
nclass = len(alphabet) + 1

onnx_weight = '20200725_crnn_sevt.onnx'
dynamic_onnx_weight = sevtconfig.crnn_checkpoint_onnx

model = crnn.CRNN64(imgH, num_channel, nclass, 256)
model.load_state_dict(torch.load(
    sevtconfig.crnn_checkpoint, map_location='cpu'))
model.eval()

dummy_input = torch.randn(2, num_channel, imgH, 1200)
torch.onnx.export(model, dummy_input, onnx_weight)

model = onnx.load(onnx_weight)
model.graph.input[0].type.tensor_type.shape.dim[0].dim_param = '?'
model.graph.input[0].type.tensor_type.shape.dim[3].dim_param = '?'
onnx.save(model, dynamic_onnx_weight)
