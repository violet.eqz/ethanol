import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="classifier CRNN",
    version="0.0.1",
    author="cuongnd",
    author_email="nguyenduycuong2004@gmail.com",
    description="Project for AICR of vision team",
    install_requires=[],
    url="",
    packages=setuptools.find_packages(),
    package_data={'classifier_crnn': []},
    include_package_data=True,
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
)
