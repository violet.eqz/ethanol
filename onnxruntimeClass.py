import time
import numpy as np
import onnxruntime as rt
import torch
from scipy.special import softmax as softmax
from torch.autograd import Variable
from torchvision import transforms
from torchvision.transforms import ToTensor, Normalize

# import classifier_crnn.models.crnn as crnn
import models.utils as utils
from utils.augment_functions2 import cnd_aug_resizePadding
from data.char_list import alphabet
from models.utils import strLabelConverter
import config_crnn as config_crnn
from utils.loader import NumpyListLoader, ImageFileLoader

mean = [0.485, 0.456, 0.406]
std = [0.229, 0.224, 0.225]
fill_color = (255, 255, 255)  # (209, 200, 193)
debug = False
default_char_list = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz| ' \
                    '0123456789' \
                    'ĂÂÊÔƠƯÁẮẤÉẾÍÓỐỚÚỨÝÀẰẦÈỀÌÒỒỜÙỪỲẢẲẨĐẺỂỈỎỔỞỦỬỶÃẴẪẼỄĨÕỖỠŨỮỸẠẶẬẸỆỊỌỘỢỤỰỴ' \
                    'ăâêôơưáắấéếíóốớúứýàằầèềìòồờùừỳảẳẩđẻểỉỏổởủửỷãẵẫẽễĩõỗỡũữỹạặậẹệịọộợụựỵ' \
                    '\'*:,@.-(#%")/~!^&_+={}[]\;<>?※”$€£¥₫°²™ā– '

inv_normalize = transforms.Normalize(
    mean=[-0.485 / 0.229, -0.456 / 0.224, -0.406 / 0.225],
    std=[1 / 0.229, 1 / 0.224, 1 / 0.225])


# rt.InferenceSession('/home/ntanh/data/crnn.onnx')

class Classifier_CRNN_onnx:
    def __init__(self, ckpt_path='', gpu='0', batch_sz=16, workers=4, num_channel=3, imgW=1024, imgH=64, model_h=64,
                 char_list=default_char_list, debug=False):
        self.gpu = gpu
        if gpu is not None:
            print('Classifier_CRNN_onnx. Use GPU', gpu)
        else:
            print('Classifier_CRNN_onnx. Use CPU')
        self.imgW = imgW
        self.imgH = imgH
        self.batch_sz = batch_sz
        # nclass = len(char_list) + 1
        self.image = torch.FloatTensor(batch_sz, num_channel, imgH, imgH)
        self.text = torch.IntTensor(batch_sz * 5)
        self.length = torch.IntTensor(batch_sz)
        self.debug = debug
        # self.model = crnn.CRNN64(imgH, num_channel, nclass, 256)
        self.sess = rt.InferenceSession(ckpt_path)
        print('Classifier_CRNN_onnx. Load checkpoint %s' % ckpt_path)
        # self.model.load_state_dict(torch.load(global_config.crnn_checkpoint, map_location='cpu'))
        self.converter = strLabelConverter(char_list, ignore_case=False)
        self.image = Variable(self.image)
        self.text = Variable(self.text)
        self.length = Variable(self.length)
        self.workers = workers
        # self.model.eval()

    def inference(self, *args):
        if len(args) == 1:
            numpy_img_list = args[0]
            max_wh_ratio = 2
            for img in numpy_img_list:
                wh_ratio = img.shape[1] / img.shape[0]
                if wh_ratio > max_wh_ratio:
                    max_wh_ratio = wh_ratio
            new_W = int(self.imgH * max_wh_ratio)
            print('Classifier_CRNN_onnx. New W', new_W)
            transform_test = transforms.Compose([
                cnd_aug_resizePadding(
                    new_W, self.imgH, fill=fill_color, train=False),
                ToTensor(),
                Normalize(mean, std)
            ])

            val_dataset = NumpyListLoader(
                numpy_img_list, transform=transform_test)
        if len(args) == 3:
            dir, file_list, label = args[0], args[1], args[2]
            print('Classifier_CRNN_onnx. W', self.imgW)
            transform_test = transforms.Compose([
                cnd_aug_resizePadding(
                    self.imgW, self.imgH, fill=fill_color, train=False),
                ToTensor(),
                Normalize(mean, std)
            ])
            val_dataset = ImageFileLoader(
                dir, flist=file_list, label=label, transform=transform_test)
        return self.run_inference(val_dataset)

    def run_inference(self, val_dataset):
        num_files = len(val_dataset)
        print('Classifier_CRNN_onnx. Begin classify', num_files, 'boxes')
        text_values = []
        prob_value = []
        val_loader = torch.utils.data.DataLoader(
            val_dataset,
            batch_size=self.batch_sz,
            num_workers=self.workers,
            shuffle=False
        )

        val_iter = iter(val_loader)
        max_iter = len(val_loader)
        # begin = time.time()
        # with torch.no_grad():
        for i in range(max_iter):
            data = val_iter.next()
            cpu_images, cpu_texts, _ = data
            batch_size = cpu_images.size(0)
            utils.loadData(self.image, cpu_images)
            # start = time.time()
            # preds = self.model(self.image)
            # print('pytorch predict time:', time.time() - start)
            temp_img = self.image.clone()
            input_batch = temp_img.cpu().detach().numpy()
            input_batch = {'input.1': input_batch}
            start = time.time()
            preds_onnx = self.sess.run(None, input_batch)
            # print('Classifier_CRNN_onnx. Predict time:', time.time() - start)
            # print(preds)
            # print("shape:     ", preds.shape)
            # print("shape_onnx:", preds_onnx[0].shape)
            # ps =
            # print('shape pytorch softmax:', softmax(preds, dim=-1).cpu().detach().numpy())
            # print('shape scipy   softmax:', sc_softmax(preds_onnx[0], axis=-1))
            # sub_sm = np.subtract(softmax(preds, dim=-1).cpu().detach().numpy(), sc_softmax(preds_onnx[0], axis=-1))
            # print('subtract:', sub_sm)
            # print('max:', np.max(sub_sm))
            # print('shape pytorch softmax max:', softmax(preds, dim=-1).max(-1)[0])
            # print('shape scipy   softmax max:', sc_softmax(preds_onnx[0], axis=-1).max(axis=-1))
            # max_index = sc_softmax(preds_onnx[0], axis=-1).argmax(axis=-1)
            # print('shape pytorch softmax max indices:', softmax(preds, dim=-1).max(-1)[1])
            # print('shape scipy   softmax max indices:', max_index)
            # sub_sm = np.subtract(softmax(preds, dim=-1).max(-1)[1], max_index)
            # print('subtract:', sub_sm)
            values_np = softmax(preds_onnx[0], axis=-1).max(axis=-1)
            prob_np = softmax(preds_onnx[0], axis=-1).argmax(axis=-1)
            preds_idx_np = (prob_np > 0).nonzero()
            a = np.reshape(preds_idx_np[0], (-1, 1))
            b = np.reshape(preds_idx_np[1], (-1, 1))
            preds_idx_np = np.concatenate((a, b), axis=1)
            # print('preds_idx_np:', preds_idx_np)
            # sent_prob_np = values_np[preds_idx_np].mean().item()
            # print('sent_prob_np:', sent_prob_np)
            psm_np = softmax(preds_onnx[0], axis=2)
            preds_onnx = preds_onnx[0].argmax(axis=2)
            preds_size_np = np.array([preds_onnx.shape[0]] * batch_size)
            # print('preds_size_np:', len(preds_size_np))
            preds_onnx = preds_onnx.transpose(1, 0).reshape(-1)
            # print('preds_onnx:', preds_onnx)
            new_psm_np = psm_np.max(axis=2)
            new_psm_np = new_psm_np.transpose(1, 0).reshape(-1)
            sim_pred, confidences = self.converter.decode_conf_onnx(
                preds_onnx, preds_size_np, new_psm_np, raw=False)
            # print(sim_pred)
            text_values.extend(sim_pred)
            # prob_value.extend([sent_prob_np])

            if any(isinstance(el, list) for el in confidences):
                prob_value.extend(confidences)
            else:
                prob_value.append(confidences)

        del val_dataset
        del val_loader
        return text_values, prob_value


def demo():
    engine = Classifier_CRNN_onnx(ckpt_path=config_crnn.pretrained_test_onnx, batch_sz=config_crnn.batch_size_test,
                                  imgW=config_crnn.imgW, imgH=config_crnn.imgH, gpu=None,
                                  char_list=alphabet, debug=config_crnn.debug)

    begin = time.time()
    _, _ = engine.inference(config_crnn.test_dir,
                            config_crnn.test_list, config_crnn.label)
    end = time.time()
    print('Inference time:', end-begin, 'seconds')


if __name__ == "__main__":
    demo()
