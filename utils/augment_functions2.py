import cv2
import random
import numpy as np
from PIL import Image


def augment_image_addline_simple(img, size_draw=1, sizelinedot=1):
    type = random.randint(-1, 2)
    black_color = (10, 10, 10)
    red_color = (70, 127, 209)
    white_color = (255, 255, 255)
    rand_color = (random.randint(0, 255), random.randint(0, 255), random.randint(0, 255))
    range_dot = random.randint(5,15)
    #logger.info('augment_image_addline_simple. Type', type)
    # type <0 solid line, 1 dots line,2 line dot
    h_img = img.shape[0]
    w_img = img.shape[1]

    h_average = random.randint(int(h_img / 5), int(4 * h_img / 5))
    size_draw = max(int(size_draw / 3), 1)
    #logger.info('size draw',size_draw)

    if type <= 0:
        draw_color = random.choice([black_color, red_color, red_color, white_color, rand_color]) #double red color
        #logger.info(draw_color)
        cv2.line(img, (0, h_average - 1), (w_img, h_average - 1), draw_color, size_draw)
        cv2.line(img, (0, h_average), (w_img, h_average), draw_color, size_draw)
        cv2.line(img, (0, h_average + 1), (w_img, h_average + 1), draw_color, size_draw)
    else:
        draw_color = random.choice([black_color, red_color, rand_color])
        #logger.info(draw_color)
        for i in range(int(w_img / range_dot)):
            if type == 1:
                centerx = int((i * range_dot) + int(size_draw / 2))
                centery = h_average - int((size_draw) / 2)
                cv2.circle(img, (centerx, centery), size_draw, draw_color, -1,cv2.LINE_AA)
            elif type == 2:
                size_line = int(range_dot / sizelinedot)
                beginl = int(i * range_dot) + int(i * size_line)
                endl = beginl + size_line
                cv2.line(img, (beginl, h_average - 1), (endl, h_average - 1), draw_color, size_draw)
                cv2.line(img, (beginl, h_average), (endl, h_average), draw_color, size_draw)
                cv2.line(img, (beginl, h_average + 1), (endl, h_average + 1), draw_color, size_draw)
    return img

def augment_resizePadding(img, width, height, fill=(255, 255, 255), train=True):
    desired_w, desired_h = width, height  # (width, height)
    img_w, img_h = img.size  # old_size[0] is in (width, height) format
    stretch_scale = 1.0
    if train:
        stretch_scale = random.uniform(2 / 3, 2)
    ratio = stretch_scale * img_w / img_h
    new_w = int(desired_h * ratio)
    new_w = new_w if desired_w == None else min(desired_w, new_w)
    if new_w>0 and desired_h>0:
        img = img.resize((new_w, desired_h), Image.ANTIALIAS)
    else:
        print('augment_resizePadding. error. new_w',new_w,'desired_h',desired_h)
        img = img.resize((width, height), Image.ANTIALIAS)

    # padding image
    if desired_w != None and desired_w > new_w:
        new_img = Image.new("RGB", (desired_w, desired_h), color=fill)
        new_img.paste(img, (0, 0))
        img = new_img

    return img

def augment_blur(img, type_blur=0, size_window=5):
    # type 0 average bluer, 1 gaussian blur, 2 median blur
    img_rs = None
    if type_blur == 0:
        img_rs = cv2.blur(img, (size_window, size_window))
    elif type_blur == 1:
        img_rs = cv2.GaussianBlur(img, (size_window, size_window), 0)
    else:
        img_rs = cv2.medianBlur(img, size_window)
    return img_rs

class cnd_aug_resizePadding(object):
    def __init__(self, width, height, fill=(255, 255, 255), train=True):
        self.width = width
        self.height = height
        self.fill = fill
        self.train = train

    def __call__(self, img):  # img is PIL image
        img_rs = augment_resizePadding(img, self.width, self.height, self.fill, self.train)
        return img_rs


class cnd_aug_add_line(object):
    def __init__(self, size_draw=1, sizelinedot=3):
        # type 0 solid line, 1 dots line,2 line dot
        self.size_draw = size_draw
        self.sizelinedot = sizelinedot

    def __call__(self, img):
        numpy_image = np.array(img)
        cv_img = cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
        img_rs = augment_image_addline_simple(cv_img, self.size_draw, self.sizelinedot)
        img_rs = cv2.cvtColor(img_rs, cv2.COLOR_BGR2RGB)
        return Image.fromarray(img_rs)

class cnx_aug_blur(object):
    def __init__(self, type_blur=0):
        # type 0 average bluer, 1 gaussian blur, 2 median blur
        self.type_blur = type_blur

    def __call__(self, img):
        numpy_image = np.array(img)
        size_window = random.choices([3,5])
        cv_img = cv2.cvtColor(numpy_image, cv2.COLOR_RGB2BGR)
        img_rs = augment_blur(cv_img, self.type_blur, size_window[0])
        img_rs = cv2.cvtColor(img_rs, cv2.COLOR_BGR2RGB)
        return Image.fromarray(img_rs)