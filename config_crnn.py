# common
imgW = 256
imgH = 64

# train
train_dir = '/data20.04/data/aicr/ocr_dataset'
output_dir = '/data20.04/data/aicr_checkpoints/classifier_crnn/training'
pretrained = '/data20.04/data/aicr_checkpoints/classifier_crnn/pretrained/' \
             '20200411_AICR_train_ocr_dataset_new_augment_char_238_64_39_loss_3.7_cer_0.0266.pth'
pretrained = ''
gpu_train = '1'  # '0,1' or None
backbone = "VGG_like"  # resnet | VGG_like
base_lr = 0.001
max_epoches = 1000
workers_train = 8
batch_size = 64
train_file = 'train_ocr_dataset.txt'
val_file = 'val_ocr_dataset.txt'
ckpt_prefix = '20200912_ocr_dataset_64_'+backbone

# test
# test_dir = '/usb/sdb/data/aicr/korea_test_set/Eval_Vietnamese/crnn2'
test_dir = '/data_backup/cuongnd/Viettel_freeform/MB_test/crop_hw'
pretrained_test = '/usb/sdb/data/aicr_checkpoints/classifier_crnn/training/' \
                  'train_2020-08-15_16-35_general_vnmese_eng/20200815_general_vnmese_english_52_loss_0.06_cer_0.0061.pth'
pretrained_test = '/usb/sdb/data/aicr_checkpoints/classifier_crnn/pretrained/20200725_sevt_64_262_loss_0.99_cer_0.0079.pth'
pretrained_test_onnx = '/home/cuongnd/PycharmProjects/aicr/aicr.core2/aicr_configs/aicr_configs/sevt/checkpoints/' \
    'classifier_crnn/20200906_SEVT_unplan_159_loss_0.05_cer_0.0016.onnx'
pretrained_test = '/home/cuongnd/PycharmProjects/classifier_crnn/classifier_crnn/weights/20200927_sevt_64_VGG_like_100_loss_0.06_cer_0.0021.pth'
# for testing SDV printing character cases
label = False
test_list = 'val_hw.txt'
test_list = ''

gpu_test = '0'
workers_test = 4
batch_size_test = 1
debug = False
write_predict_file = False
include_conf = False  # write predict result + conf
if debug:
    batch_size_test = 1
